"""d_p_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from travelo.views import index_static,index_dynamic
from django.conf import settings
from django.conf.urls.static import static


#path('travelo/',include('travelo.urls'))
#path('app1/',include('app1.urls'))

urlpatterns = [
    path('',include('testingapp.urls')),
    path('travelo_static/',index_static,name='index_static_at_root'),
    path('travelo_dynamic/',index_dynamic, name='index_dynamic_at_root'),
    path('admin/', admin.site.urls),
    path('travelo_dynamic/',include('accounts.urls')),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)



