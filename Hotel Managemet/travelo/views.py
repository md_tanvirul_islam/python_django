from django.shortcuts import render
from django.http import HttpResponse
from .models import Destination

# Create your views here.


def index_static(request):
    #return HttpResponse("i am tanvir travelo view")
    return render(request,'index_static.html')


def index_dynamic(request):
    destinations = Destination.objects.all()
    return render(request,'index_dynamic.html',{'destinations':destinations})



    # ******for static file and static way of rendering website content.
    # destination1 = Destination()
    # destination1.name = 'Dhaka'
    # destination1.description = 'The capital of Bangladesh'
    # destination1.price = 3000
    # destination1.image = 'dhaka1.jpg'
    # destination1.offer = True
    #
    # destination2 = Destination()
    # destination2.name = 'Khulna'
    # destination2.description = " The gateway to the Sundarbans."
    # destination2.price = 4000
    # destination2.image = 'khulna1.jpg'
    # destination2.offer = False
    #
    # destination3 = Destination()
    # destination3.name = 'Sylhet'
    # destination3.description = 'Place for wonderful tea gardens and waterfalls'
    # destination3.price = 4500
    # destination3.image = 'sylhet1.jpg'
    # destination3.offer = False
    #
    # destination4 = Destination()
    # destination4.name = "Cox's Bazar"
    # destination4.description = "Longest sea beach in world"
    # destination4.image = 'coxsbazar1.jpg'
    # destination4.price = 5000
    # destination4.offer = True
    #
    # destination5 = Destination()
    # destination5.name = "Comilla"
    # destination5.description = "famous for Rosmuli"
    # destination5.image = 'comilla1.jpg'
    # destination5.price = 4000
    # destination5.offer = False
    #
    # destination6 = Destination()
    # destination6.name = "Rajshai"
    # destination6.description = "famous for Mango and sweet"
    # destination6.image = 'rajshai1.jpg'
    # destination6.price = 3500
    # destination6.offer = False
    #
    # des_list= [destination1,destination2,destination3]
    #des_list = [destination1,destination2,destination3,destination4,destination5,destination6]
    #return render(request, 'index_dynamic.html', {'destinations': des_list})
    # return render(request, 'index_dynamic.html', {'name':'dhaka','price':200,'des':'hello des'})

