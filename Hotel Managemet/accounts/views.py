from django.shortcuts import render,HttpResponseRedirect
from django.contrib.auth import authenticate,login as dj_login,logout as dj_logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponse
from django.urls import reverse
# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        #return HttpResponse(print(user))
        
        if user is not None:
            dj_login(request,user)
            return HttpResponseRedirect("/travelo_dynamic")   
        else:
            messages.error(request,'Email or Password do not Match')
            return HttpResponseRedirect("login")
    else:
        return render(request,'login.html')


def register(request):

    if request.method == 'POST':
        u_first_name = request.POST['first_name']
        u_last_name = request.POST['last_name']
        u_username = request.POST['username']
        u_password1 = request.POST['password1']
        u_password2 = request.POST['password2']
        u_email = request.POST['email']

        if u_password1==u_password2:
                if User.objects.filter(username=u_username).exists():
                    messages.info(request,'username is taken')
                    return HttpResponseRedirect("register")


                elif User.objects.filter(email=u_email).exists():
                    messages.info(request, 'Email is taken')
                    return HttpResponseRedirect("register")

                else:
                    user = User.objects.create_user(username=u_username,password=u_password1,email=u_email,first_name=u_first_name,last_name= u_last_name)
                    user.save()
                    print('user created')
                    return HttpResponseRedirect("login")

        else:
            messages.info(request, ' Both Password are not same')
            return HttpResponseRedirect("register")
        return HttpResponseRedirect("travelo_dynamic")

    else:
        return render(request,'register.html')

def logout(request):
    dj_logout(request)
    return HttpResponseRedirect("/travelo_dynamic")



# user= auth.authenticate(email='absaman@gmail.com',password='00000')
# print(user)